document.addEventListener("DOMContentLoaded", () => {
    const usernameData = sessionStorage.getItem("Username");

    if(usernameData){
        document.getElementById("userName").innerText = usernameData;
    }
    else{
        window.location.href = "../pages/login.html";
    }
});

function LogOut(){
    sessionStorage.removeItem("Username");
    sessionStorage.removeItem("Jwt_Secure");
    
    window.location.href = "../pages/login.html";
}