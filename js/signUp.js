const baseApiAccountAddress = "http://localhost:5073/api/account";
const registrationForm = document.getElementById("registrationForm");

registrationForm.addEventListener("submit", async (event) => {
    event.preventDefault();

    const formData = new FormData(registrationForm);

    let user = {
        username: formData.get("userInputName").trim(),
        email : formData.get("userInputEmail").trim(),
        password : formData.get("userInputPassword").trim(),
        confirmPassword : formData.get("userInputConfirmPassword").trim()
    };

    if (!isValidForm(user)){
        return false;
    }

    var data = await registerAsync(user);

    if (data){
        window.location.href = "../pages/login.html";
    }
});

async function registerAsync(user){
    try {
        const response = await fetch(baseApiAccountAddress + '/auth/register', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
        });

        if (response.ok){
            return await response.json();
        }
        else {
            let errorData = await response.json();

            throw new Error(`"${errorData.message}"`);
        }
    }
    catch (error){
        alert(`Warning: ${error.message}`);
    }
}

function isValidForm(user){
    if(!user.username){
        alert("Будь ласка, введіть коректне ім'я користувача.");
        return;
    }

    if(!validateEmail(user.email)){
        alert('Будь ласка, введіть дійсну електронну адресу.');
        return false;
    }

    if (user.password.length < 8) {
        alert('Пароль повинен містити щонайменше 8 символів.');
        return false;
    }

    if (user.password !== user.confirmPassword) {
        alert('Пароль підтвердження повинен співпадати.');
        return false;
    }

    return true;
}

function validateEmail(email) {
    const pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return pattern.test(String(email).toLowerCase());
}
        