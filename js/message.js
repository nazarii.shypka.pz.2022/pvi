const serverAddress = "http://localhost:3000/";
const newChatForm = document.getElementById("newChatForm");
const addMemberForm = document.getElementById("addMemberForm");
const sendUserMessage = document.getElementById("sendUserMessage");
const userInputMessage = document.getElementById("userInputMessage");
const chatName = document.getElementById("chatName");
let userNameInfo = null;

let socket = io(serverAddress);

window.addEventListener('load', () => {
	userNameInfo = sessionStorage.getItem("Username");;

	socket.emit('logIn', userNameInfo);
});

socket.on('restoreRooms', function(rooms, readFlags) {
	console.log(rooms);
	restoreRooms(rooms);

	let chatItems = document.getElementById('chatItems').children;

	for (let i = 0; i < chatItems.length; i++)
	{
		if (chatItems[i].classList.contains('chatItemSelected')){
			continue;
		}

		if (readFlags[i] === false){
			chatItems[i].className ='chatItemNotRead';
		}
	} 
})

socket.on('restoreChatHistory', function(data) {
	restoreChat(data);
})

socket.on('update', function(data) {
	appendOtherMessage(data);
	let history = document.getElementById("chatHistory");
	history.scrollTop = history.scrollHeight;
})

socket.on('createNewChatStatusError', function() {
	alert("Chat with this name already exists");
})

socket.on('addNewMemberStatusError', function() {
	document.getElementById('chatMembers').removeChild(document.getElementById('chatMembers').lastChild);
	alert("Member already presented");
})

socket.on('updateNotRead', function(roomName) {
	console.log(roomName);
	let chatItems = document.getElementById('chatItems').children;
	for (let i = 0; i < chatItems.length; i++)
	{
		if (chatItems[i].classList.contains('chatItemSelected')){
			continue;
		}
			
		if (chatItems[i].children[1].innerHTML === roomName){
			chatItems[i].className ='chatItemNotRead';
		}
	} 
	console.log(roomName);
})

document.getElementById("showAddMemberModal").onclick = async () => {

	let data = await getAllUsersAsync();

	if (data){
		console.log();

		let element = document.getElementById("newMember");

		for (let i = 0; i < data.length; i++){
			if (data[i].userName === userNameInfo){
				continue;
			}

			let option = document.createElement('option');
			option.value = data[i].userName;
			option.innerHTML = data[i].userName;
			element.appendChild(option);
		}

		console.log(data);
	}
}

async function getAllUsersAsync(){
	try {
		let jwtToken = sessionStorage.getItem("Jwt_Secure");

        const response = await fetch('http://localhost:5073/api/account/all', {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
				"Authorization": `Bearer ${jwtToken}`
            }
        });

        if (response.ok){
            return await response.json();
        }
        else {
            let errorData = await response.json();

            throw new Error(`"${errorData.message}"`);
        }
    }
    catch (error){
        alert(`Warning: ${error.message}`);
    }
}

sendUserMessage.onclick = async () => {
	let messageText = userInputMessage.value.trim();
	sendMessage(messageText);
}

newChatForm.addEventListener("submit", async (event) => {
    event.preventDefault();

	const formData = new FormData(newChatForm);

	let chatName = formData.get("newChatName").trim();

	socket.emit('createNewChat', chatName, userNameInfo);

	hideAddChatModal();
});

addMemberForm.addEventListener("submit", async (event) => {
    event.preventDefault();

	const formData = new FormData(addMemberForm);

	let newMemberName = formData.get("newMember").trim();

	socket.emit('addNewMember', newMemberName, chatName.innerHTML);
	addMemberIcon(newMemberName);

	hideAddMemberModal();
});

function addChatElement(chatName)
{
	let chatItems = document.getElementById("chatItems");
	let chatItem = chatItems.appendChild(document.createElement("div"));
	chatItem.classList.add("chatItem");

	chatItem.onclick = function(event) {
		selectRoom(event);
	}

	chatItem.innerHTML = 
	"<div class ='chatIcon'></div>\
	<div class = 'chatName'>"+chatName+"</div>";
}

function sendMessage(text){
	const message = {from: userNameInfo, text: text};

	appendOwnMessage(message);
	socket.emit('send', message);

	let history = document.getElementById("chatHistory");
	history.scrollTop = history.scrollHeight;

	userInputMessage.value="";
}

function appendOwnMessage(message)
{
	console.log(message);

	text = sanitizeElementSymbols(message.text);
	let history = document.getElementById("chatHistory");

	history.insertAdjacentHTML("beforeend",
	`<div class ="messageContainer my-message">
		<div class="userInfo">
			<div class="authorIcon" title="${message.from}"></div>
			<div class="authorName">Me</div>
		</div>
		<div class="message">
			<div class="talktext">
	 			${text}
			</div>
		</div>
	</div>`);
}

function appendOtherMessage(message)
{
	text = sanitizeElementSymbols(message.text);
	let history = document.getElementById("chatHistory");
	history.insertAdjacentHTML("beforeend",
	`<div class ="messageContainer other-message">
		<div class="userInfo">
			<div class="authorIcon" title="${message.from}"></div>
			<div class="authorName">${message.from}</div>
		</div>
		<div class="message">
			<div class="talktext">
	 			${text}
			</div>
		</div>
	</div>`);
}

function clearChatView()
{
	document.getElementById("chatMembers").innerHTML = "";
	document.getElementById("chatHistory").innerHTML = "";
}

function addMemberIcon(name)
{
	let history = document.getElementById("chatMembers");
	history.insertAdjacentHTML("beforeend", "<div class ='memberIcon' title = '"+name +"'></div>");
}

function restoreRooms(rooms) {
	let chatItems = document.getElementById('chatItems').children;
	let currentSelected = -1;
	for (let i = 0; i < chatItems.length; i++)
	{
		if (chatItems[i].classList.contains('chatItemSelected'))
			currentSelected = i;
	} 
	document.getElementById("chatItems").innerHTML = "";
	for (let i =0; i < rooms.length; i++)
	{
		addChatElement(rooms[i]);
	}
	let chatItems2 = document.getElementById('chatItems').children;
	if (currentSelected > chatItems2.length-1 || currentSelected === -1)
		return;
	chatItems2[currentSelected].className = "chatItemSelected";
}

function restoreChat(data) {
	clearChatView();

	chatName.innerHTML = data.roomName;
	for (let i = 0; i < data.members.length; i++)
	{
		addMemberIcon(data.members[i]);
	}
	for (let i = 0; i < data.messages.length; i++)
	{
		if (data.messages[i].from === userNameInfo)
			appendOwnMessage(data.messages[i]);
		else
			appendOtherMessage(data.messages[i]);
	}
	let history = document.getElementById("chatHistory");
	history.scrollTop = history.scrollHeight;
}

function selectRoom(event)
{
	let roomName = updateSelectedChatItem(event);

	showChatView();

	console.log(roomName);
	socket.emit('requestRoomHistory', roomName);
}

function showChatView(){
	document.getElementById("chatView").style.display = "flex";
}

function updateSelectedChatItem(event)
{
	let children = document.getElementById("chatItems").children;
	let selectedIndex = -1;
	for (let i = 0; i < children.length; i++)
	{
		if (selectedIndex != -1 )
			break;
		if (event.target === children[i])
		{
			selectedIndex = i;
			break;
		}
		else
		{
			let grandchildren = children[i].children;
			for (let j = 0; j < grandchildren.length; j++)
			{
				if (event.target === grandchildren[j])
				{
					selectedIndex = i;
					break;
				}
			}
		}
	}

	for (let i = 0; i < children.length; i++)
	{
		if (children[i].classList.contains("chatItemSelected"))
		{
			children[i].className = "chatItem";
		}
	}

	children[selectedIndex].className = "chatItemSelected";

	return children[selectedIndex].children[1].innerHTML;
}

function sanitize(string)
{
	let sanitizedString = string.replace(/</g, "&lt").replace(/>/g, "&gt").replace(/ /g, "&nbsp");
	return sanitizedString;
}

function sanitizeElementSymbols(string)
{
	let sanitizedString = string.replace(/</g, "&lt").replace(/>/g, "&gt");
	return sanitizedString;
}

function hideAddChatModal(){
    $("#newChatModal").modal("hide");
    newChatForm.reset();
}

function hideAddMemberModal(){
    $("#addMemberModal").modal("hide");
    addMemberForm.reset();
}