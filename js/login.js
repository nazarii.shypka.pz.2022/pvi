const baseApiAccountAddress = "http://localhost:5073/api/account";
const loginForm = document.getElementById("loginForm");

loginForm.addEventListener("submit", async (event) => {
    event.preventDefault();

    const formData = new FormData(loginForm);

    let user = {
        username: formData.get("userInputName").trim(),
        password : formData.get("userInputPassword").trim(),
    };

    if (!isValidForm(user)){
        return false;
    }

    let data = await logInAsync(user);

    if (data){
        sessionStorage.setItem("Username", user.username);
        sessionStorage.setItem("Jwt_Secure", data.accessToken);

        window.location.href = "../pages/index.html";
    }
});

async function logInAsync(user){
    try {
        const response = await fetch(baseApiAccountAddress + '/auth/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",

            },
            body: JSON.stringify(user)
        });

        if (response.ok){
            return await response.json();
        }
        else {
            let errorData = await response.json();

            throw new Error(`"${errorData.message}"`);
        }
    }
    catch (error){
        alert(`Warning: ${error.message}`);
    }
}

function isValidForm(user){
    if(!user.username){
        alert("Будь ласка, введіть коректне ім'я користувача.");
        return false;
    }

    if (user.password.length < 8) {
        alert('Пароль повинен містити щонайменше 8 символів.');
        return false;
    }

    return true;
}
        