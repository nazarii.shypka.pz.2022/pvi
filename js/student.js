class Student{
    constructor(id, name, surname, gender, birthday, group){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.birthday = birthday;
        this.group = group;
    }
}

const studentsTable = document.getElementById("studentsTable").getElementsByTagName("tbody")[0];
const modalForm = document.getElementById("modalForm");
const modalWindow = document.getElementById("modalWindow");

let studentsDb = [];

window.addEventListener('load', async () => {
    await loadAllStudentsAsync();
});

document.getElementById("addStudentModal").onclick = () => {
    setModalTitleAndButton("Add new student", "Create", "add");
}

async function loadAllStudentsAsync(){
    let students = await getAllStudentsRequestAsync();

    if (students) {
        studentsDb = students.map(studentData => new Student(
            studentData.id,
            studentData.name,
            studentData.surname,
            studentData.gender,
            new Date(studentData.birthday),
            studentData.group
        ));

        printToTableStudents(studentsDb);
    }
}

function addStudent(student){
    let newStudent = new Student(student.id, student.name, student.surname, student.gender, new Date(student.birthday), student.group);
    studentsDb.push(newStudent);
    addStudentToTable(newStudent);
}

function editStudent(student){
    let studentToUpdate = studentsDb.find(s => s.id == student.id);

    if (studentToUpdate){
        studentToUpdate.name = student.name;
        studentToUpdate.surname = student.surname;
        studentToUpdate.gender = student.gender;
        studentToUpdate.birthday = new Date(student.birthday);
        studentToUpdate.group = studentToUpdate.group;

        printToTableStudents(studentsDb);
    }
}

function removeStudent(id, row) {
    let index = studentsDb.findIndex(s => s.id === id);

    if (index !== -1) {
        studentsDb.splice(index, 1);
        removeRow(row);
    }
    else {
        console.error("Student not found in studentsDb array.");
    }
}

async function removeStudentModalAsync(student, row){
    const confirmation = confirm(`Are you sure you want to delete user ${student.name} ${student.surname} ?`);

    if (confirmation){
        let studentId = await deleteStudentMethodRequest(student);

        if (studentId){
            removeStudent(studentId, row);
        }
    }
}

function printToTableStudents(studentsArray){
    clearTable(studentsTable);
    studentsArray.forEach(student => addStudentToTable(student));
}

function addStudentToTable(student){
    let newRow = studentsTable.insertRow();
    newRow.innerHTML = getRowInfo(student);

    newRow.querySelector("#edit-btn").addEventListener("click", () => editStudentModal(student));
    newRow.querySelector("#remove-btn").addEventListener("click", async () => removeStudentModalAsync(student, newRow));
}

function removeRow(row){
    row.remove();
}

function clearTable(table){
    table.innerHTML = "";
}

function getRowInfo(student){
    return `<td class="student-id"><input type="checkbox"/></td>
            <td>${student.group}</td>
            <td>${student.name} ${student.surname}</td>
            <td>${student.gender}</td>
            <td>${student.birthday.toLocaleDateString()}</td>
            <td><div class="status active"></div></td>
            <td>
                <button class="blue-btn" id="edit-btn" data-bs-toggle="modal" data-bs-target="#modalWindow">
                    <img src="../images/penIcon.png" alt="Edit">
                </button>
                <button class="blue-btn" id="remove-btn">
                    <img src="../images/removeIcon.png" alt="Remove">
                </button>
            </td>
        `;
}

function editStudentModal(student){
    modalForm.elements["studentId"].readOnly = false;
    modalForm.elements["studentId"].value = student.id;
    modalForm.elements["studentId"].readOnly = true;

    modalForm.elements["studentGroup"].value = student.group;
    modalForm.elements["studentName"].value = student.name;
    modalForm.elements["studentSurname"].value = student.surname;
    modalForm.elements["studentGender"].value = student.gender;
    modalForm.elements["studentBirthday"].value = student.birthday.toISOString().split('T')[0];

    setModalTitleAndButton("Edit student", "Save changes", "edit");
}

modalForm.addEventListener("submit", async (event) => {
    event.preventDefault();

    const action = modalForm.dataset.action;
    const formData = new FormData(modalForm);

    if (action === "add"){
        let student = new Student(
            0,
            formData.get("studentName").trim(),
            formData.get("studentSurname").trim(),
            formData.get("studentGender"),
            new Date(formData.get("studentBirthday")),
            formData.get("studentGroup").trim()
        );

        if (!IsValidForm(student)){
            return false;
        }

        let newStudent = await createStudentRequestAsync(student);

        if (newStudent){
            addStudent(newStudent);
            closeModalWindow();
        }
    }
    else if (action === "edit") {
        let student = new Student(
            parseInt(formData.get("studentId")),
            formData.get("studentName").trim(),
            formData.get("studentSurname").trim(),
            formData.get("studentGender"),
            new Date(formData.get("studentBirthday")),
            formData.get("studentGroup").trim()
        );

        if (!IsValidForm(student)){
            return false;
        }

        let updatedStudent = await updateStudentRequestAsync(student);

        if (updatedStudent){
            editStudent(updatedStudent);
            closeModalWindow();
        }
    }
});

function closeModalWindow(){
    $("#modalWindow").modal("hide");
    modalForm.reset();
}

function setModalTitleAndButton(title, buttonText, action){
    modalWindow.querySelector(".modal-title").textContent = title;
    modalWindow.querySelector(".modal-footer .btn-primary").textContent = buttonText;
    modalForm.dataset.action = action;
}

function IsValidForm(student){
    if (!student.name) {
        alert("Please enter a valid first name.");
        return false;
    }

    if (!student.surname) {
        alert("Please enter a valid last name.");
        return false;
    }

    if (!student.gender){
        alert("Please enter a valid gender.");
        return false;
    }

    if (!student.birthday || isNaN(student.birthday)) {
        alert("Please enter a valid birthday.");
        return false;
    }

    if (!student.group){
        alert("Please enter a valid group.");
    }

    return true;
}

const serverStudentUrl = "http://localhost:5073/api/Student";

async function getAllStudentsRequestAsync(){
    let data = await makeRequest(serverStudentUrl, "GET");

    if (data){
        console.log(data);
        return data.students;
    }
}

async function createStudentRequestAsync(student){
    let data = await makeRequest(serverStudentUrl, "POST", student);

    if (data){
        console.log(data);
        alert(`${data.message}, id = ${data.id}`);
        return data.student;
    }

    return false;
}

async function updateStudentRequestAsync(student){
    let data = await makeRequest(serverStudentUrl, "PUT", student);

    if (data){
        console.log(data);
        alert(`${data.message}, id = ${data.id}`);
        return data.student;
    }

    return false;
}

async function deleteStudentMethodRequest(student){
    let data = await makeRequest(serverStudentUrl, "DELETE", student);

    if (data){
        console.log(data);
        alert(`${data.message}, id = ${data.id}`);
        return data.id;
    }

    return false;
}

async function makeRequest(url, method, body){
    try {
        const response = await fetch(url, {
            method: method,
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        });

        if (response.ok){
            return await response.json();
        }
        else if (response.status === 400 || response.status === 404 || response.status === 409 || response.status === 422){
            const errorData = await response.json();
            console.log(errorData);
            throw new Error(errorData.message);
        }
        else if (response.status === 500){
            const errorData = await response.json();
            console.log(errorData);

            throw new Error(`Server error : ${errorData.message}`);
        }
        else {
            throw new Error(`Error status: ${response.status}`);
        }
    }
    catch (error){
        alert(`Warning: ${error.message}`);
    }
}