const PORT = 3000;
const express = require('express');
const cors  = require('cors');
const { Socket } = require('socket.io');

const connectToMongoDB = require('./db/connectToMongoDb.js');
const Chat = require("./models/chat.model.js");

const { createChat, getChatByRoomName, getRoomsContainingName, getReadFlagsForName, isUserInRoom } = require('./services/chat.service.js');

let sockets = [];

let app = express();

const corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));

let server = require('http').Server(app);

let io = require('socket.io')(server,
    {
        cors:
        {
            origin: "*",
            methods: ["GET", "POST"]
        }
    }
);

server.listen(PORT, () => {
    connectToMongoDB();
    console.log(`Server is running on port ${PORT}`);
});

io.on('connection', function (socket) {
    console.log('User connected');

    socket.on('logIn', async function(name){

        for (let  i = 0; i < sockets.length; i++)
        {
            if (name === sockets[i].username){
                io.to(sockets[i].id).emit('terminate');
            }
        }

        sockets.push({username: name, id: socket.id});

        let rooms = await getRoomsContainingName(name);
        let readFlags = await getReadFlagsForName(name);
        io.to(socket.id).emit('restoreRooms', rooms, readFlags);
    });

    socket.on('requestRoomHistory', async function(roomName) {
        let rooms = io.sockets.adapter.rooms;

        for(let [key, value] of rooms)
        {
            if (key !== socket.id){
                socket.leave(key)
            }
        }

        socket.join(roomName);
        console.log(rooms);

        const roomData = await getChatByRoomName(roomName);
        console.log(roomData);

        io.to(socket.id).emit('restoreChatHistory', {members:roomData.members, messages:roomData.messages, roomName: roomData.roomName});
        let sockName;

        for (let i = 0; i < sockets.length; i++)
        {
            if (sockets[i].id === socket.id)
            {
                sockName = sockets[i].username;
            }
        }

        for (let i = 0; i < roomData.members.length; i++)
        {
            if (sockName === roomData.members[i])
            {
                const key = `readFlags.${i}`;
                await Chat.updateOne(
                    {roomName: roomData.roomName},
                    { $set: { [key]: true } }
                );
                break;
            }
        }

        return;
    });

    socket.on('send', async function(message) {
        let theRoom;

        for (const room of socket.rooms) {
            if (room !== socket.id) {
                console.log("sent to " + room);
                socket.to(room).emit('update', message);
                theRoom = room;
            }
        }

        for (let i = 0; i < sockets.length; i++)
        {
            if (await isUserInRoom(theRoom, sockets[i].username) === true)
            {
                if (socket.id !== sockets[i].id)
                {
                    io.to(sockets[i].id).emit('updateNotRead', theRoom);
                }
            }
        }

        console.log(theRoom);
        console.log(message);

        await Chat.updateOne(
            {roomName: theRoom},
            {$push: {messages: message}}
        );

        const info = await getChatByRoomName(theRoom);

        if (info){
            const clients = io.sockets.adapter.rooms.get(theRoom);
            console.log(clients);
            for (let i = 0; i < info.members.length; i++)
            {
                let socketAlreadyReads = 0;
                for (let j = 0; j < sockets.length; j++)
                {
                    if (info.members[i] === sockets[j].username)
                    {
                        if (clients.has(sockets[j].id))
                            socketAlreadyReads = 1;
                    }
                }
                if (socketAlreadyReads===1)
                    continue;
                if (message.from !== info.members[i])
                {
                    const key = `readFlags.${i}`;
                    await Chat.updateOne(
                        {roomName: theRoom},
                        {$set: { [key]: false } }
                    );
                }
            }
        }
    });

    socket.on('createNewChat', async function(name, initialUser) {
        let chatExists = 0;

        const info = await getChatByRoomName(name);

        if (info)
        {
            chatExists = 1;
        }
        if (chatExists === 0)
        {
            const someChat = new Chat({
                roomName: name,
                members: [initialUser],
                readFlags: [false],
                messages: [],
            });

            await createChat(someChat);

            for (let i = 0; i < sockets.length; i++) {
                let rooms = await getRoomsContainingName(sockets[i].username);
                let readFlags = await getReadFlagsForName(sockets[i].username);

                io.to(sockets[i].id).emit('restoreRooms', rooms, readFlags);
            }
        }
        else
        {
            io.to(socket.id).emit('createNewChatStatusError');
        }
    })

    socket.on('addNewMember', async function(member, roomName) {
        if (await isUserInRoom(roomName, member) === false)
        {
            await Chat.updateOne(
                {roomName: roomName},
                {$push: {members: member}}
            );

            await Chat.updateOne(
                {roomName: roomName},
                {$push: {readFlags: false}}
            );

            for (let i = 0; i < sockets.length; i++) {
                let rooms = await getRoomsContainingName(sockets[i].username);
                let readFlags = await getReadFlagsForName(sockets[i].username);
                io.to(sockets[i].id).emit('restoreRooms', rooms, readFlags);
            }

            const roomData = await getChatByRoomName(roomName);

            socket.to(roomName).emit('restoreChatHistory', {members:roomData.members, messages:roomData.messages, roomName: roomData.roomName});
        }
        else
        {
            io.to(socket.id).emit('addNewMemberStatusError');
        }
    });

    socket.on('disconnect', () => {
        console.log('User disconnected');

        for (let i = 0; i < sockets.length; i++) {
            if (sockets[i].id === socket.id) {
                sockets.splice(i, 1);
                break;
            }
        }
    });
});

