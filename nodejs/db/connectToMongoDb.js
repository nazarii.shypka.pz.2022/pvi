const connectionString = "mongodb://localhost:27017/ChatDB";
const mongoose = require("mongoose");

const connectToMongoDB = async () => {
    try {
      await mongoose.connect(connectionString);
      console.log("Connected to Mongo DB!");
    } catch (error) {
        console.log("Error connecting to MongoDB", error.message);
    }
  }
  
module.exports = connectToMongoDB;