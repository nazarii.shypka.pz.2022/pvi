const Chat = require('../models/chat.model.js');

async function createChat(chatData){
    try
    {
        return await Chat.create(chatData);
    }
    catch (error) {
        throw error;
    }
}

async function getChatByRoomName(roomName){
    try
    {
        return await Chat.findOne({roomName: roomName});
    }
    catch (error) {
        throw error;
    }
}

async function getRoomsContainingName(username)
{
    const roomsObjs = await Chat.find({members: username}, ['roomName', '-_id']);
    const rooms = roomsObjs.map(room => room.roomName);
    console.log(rooms);

    return rooms;
}

async function getReadFlagsForName(username)
{
    let flags = [];
    const info = await Chat.find({members: username}, ['members', 'readFlags']);

    for (let i = 0; i < info.length; i++)
    {
        for (let j = 0; j < info[i].members.length; j++)
        {
            if (info[i].members[j] === username)
            {
                flags.push(info[i].readFlags[j]);
                break;
            }
        }
    }
    return flags;
}

async function isUserInRoom(roomName, username)
{
    const info = await Chat.findOne({roomName: roomName, members: username});

    if (info)
    {
        console.log("yeah, "+username + " is in "+ roomName);
        return true;
    }
    console.log("No, no "+username + " in "+ roomName);

    return false;
}

module.exports = { createChat, getChatByRoomName, getRoomsContainingName, getReadFlagsForName, isUserInRoom };