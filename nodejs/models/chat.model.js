const mongoose = require("mongoose");

const chatDataSchema = new mongoose.Schema({
    roomName: String,
    members: [String],
    readFlags: [Boolean],
    messages: [{from: String, text: String}]
});
  
const Chat = mongoose.model("Chat", chatDataSchema);
  
module.exports = Chat;